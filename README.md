# Fonte de tensão

Projeto de uma fonte de tensão realizada para a disciplina de Eletrônica para Computação do curso de Ciências de Computação do ICMC-USP, ministrada por Eduardo Valle Simões.

O projeto irá iniciar com a tensão da tomada(127v ou 220v) com corrente alternada e vai transformar essa tensão em um tesão apropriada para o funcionamento da fonte que é de 3V-12v com corrente continua.


# Circuito

O circuito feito no falstad está disponível em: http://tinyurl.com/y9veyadz

![Circuito_Fonte](/uploads/863b4e7d2d6268a1ab18d41ed62afd12/Circuito_Fonte.png)

# Componentes e seus valores

| Componente | Especificação | Valor(unidade) |
| ------ | ------ | ------ |
| [Resistor](https://www.sotudo.com.br/produto/resistor-1w-5-6k-ohms) | 5,6kΩ | R$0,4 |
| [Resistor](https://www.sotudo.com.br/produto/resistor-1w-2-2k-ohms) | 2.2kΩ | R$0,40 |
| [Resistor](https://www.baudaeletronica.com.br/resistor-120r-1-2w.html) | 120Ω | R$0,08 |
| [Resistor](https://www.baudaeletronica.com.br/resistor-100r-5-1-4w.html) | 100Ω | R$0,15 |
| [Potenciômetro](https://www.baudaeletronica.com.br/potenciometro-linear-de-20k-20000.html) | 20k | R$1,53 |
| [Capacitor](https://www.baudaeletronica.com.br/capacitor-eletrolitico-1000uf-25v.html) |1000μF| R$0,80|
| [Transitor](https://www.filipeflop.com/produto/regulador-de-tensao-7815-15v/) | 15V e 500mA | R$2,40 |
| [Diodo de Zener](https://www.curtocircuito.com.br/diodo-zener-3-0v-0-5w-bzx79-c3v0.html) | 0,5W | R$0,21 |
| [Ponte Retificadora](https://www.autocorerobotica.com.br/2w10-ponte-retificadora-1000v-2a) | 1000v e 2A | R$1,30 |
| [LED](https://www.baudaeletronica.com.br/led-difuso-3mm-vermelho.html) | Difuso 3mm | R$0,24 |
| [Transformador](https://www.filipeflop.com/produto/transformador-trafo-15v-500ma-bivolt/) | 15V e 500mA | R$25,90 |
| [Switch](https://www.ryndackcomponentes.com.br/micro-switch-fim-de-curso/1342-chave-micro-switch-250v-5a-com-alavanca-de-17mm-e-roda-kw11-3z-5a-fim-de-curso.html) | 250V e 3A | R$1,72 |

# Justificativa dos Componentes

**Led:** Para mostrar se o circuito está ligado

**Transformador:** Para ser possível a troca entre 127v e 220v

**Ponte Retificadora:** Para transformar a corrente alternada em corrente continua

**Diodo de Zener:** Para ajusatar a voltagem necessaria para saída, para saber o diodo de zener necessário é só aplicar a formula P=IU, para o diodo usado no projeto P=13V*0,02A=0,26w, portando um diodo com 0,5w é o suficiente para realizar o controle de voltagem 

**Transitor:** Para realizar o controle da corrente

**Capacitor:** O pico do projrto chega a 41v, por possuir tensão de 220v, portendo é necessário um capacitor de 50v

**Resistores:**

- **5kΩ:** Para regular a tensão de base que vai para o transitor

- **2.2kΩ:** Para limitar a corrente que passa pelo diodo de zener

- **120Ω:** Para auxiliar na regulagem de tensão de saída

- **100Ω:** Para limitar a corrente que sai do circuito para 100mA

# Projeto do Esquemático e do PCB no EAGLE

# Participantes
Beatriz Aparecida Diniz - [beatrizdiniz](https://gitlab.com/beatrizdiniz)